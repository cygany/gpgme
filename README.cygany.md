This is the gpgme package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which is a debian overlay over cygwin.

Our cygany branch tracks "debian/unstable" branch in the salsa repository, roughly so:

Git remotes:
```
origin   git@gitlab.com:cygany/gpgme.git (fetch)
origin   git@gitlab.com:cygany/gpgme.git (push)
upstream https://salsa.debian.org/debian/gpgme.git (fetch)
upstream https://salsa.debian.org/debian/gpgme.git (push)
```

Branch setup:
```
* cygany          c0deebe [origin/cygany] ...
  debian/unstable fce2ea3 [upstream/debian/unstable] ...
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary -j8
```

(The "-j8" is optional, but contrary the upstream, we love parallel builds.)

But soon it will be hopefully better.
